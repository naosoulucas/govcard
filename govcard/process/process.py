import pandas as pd
import os

def get_data(month):
    """ Retorna DataFrame com dados do mês selecionado """
    return pd.DataFrame()

def get_total(df):
    """ Retorna total de despesas """
    return 1000

def get_min(df):
    """ Retorna despesa mínima """
    return 2

def get_max(df):
    """ Retorna despesa máxima """
    return 1800

def get_portador(df):
    """ Retorna os 5 portadores que mais gastaram """
    return pd.DataFrame()

def get_gestora(df):
    """ Retorna as 5 gestoras que mais gastaram """
    return pd.DataFrame()
